package staging.com.PigeonLab.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import staging.com.PigeonLab.base.Base;

public class AudienceWebApp extends Base{
	


	public AudienceWebApp()
	{
		PageFactory.initElements(driver, this);
		
	}
	
	
	
	///----------------------------Page Factory -----------------------------------------/////	
	
		@FindBy(xpath="//a[@class='sessionlist-item-enter-button session-qna']")
		WebElement EnterQandA;

		
		@FindBy(xpath="//textarea[@class='question-input question-input-no-radius']")
		WebElement EnterQuestion;

		@FindBy(xpath="//div[@class='question-input-btn-content']")
		WebElement AskButton;
		
		@FindBy(xpath="//button[@class='btn btn-primary modal-btn modal-btn-ok']")
		WebElement SubmitQuestion;

		///----------------------------Page Factory -----------------------------------------/////	


		///----------------------------Page Method -----------------------------------------/////	
		
		

		///----------------------------Page Method -----------------------------------------/////	
		
		public void ClickQA()
		{
			EnterQandA.click();
		}
		
	public void enterquestion()
	{
		EnterQuestion.click();
		EnterQuestion.sendKeys("how you like our application");
	}
	
	public void AskButton()
	{
		AskButton.click();
	}
	
	public void SubmitQuestion()
	{
		SubmitQuestion.click();
}
}