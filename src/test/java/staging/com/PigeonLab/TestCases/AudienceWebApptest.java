package staging.com.PigeonLab.TestCases;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import staging.com.PigeonLab.base.Base;
import staging.com.PigeonLab.pages.AudienceWebApp;
import staging.com.PigeonLab.pages.PigeonLabDashboard;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class AudienceWebApptest extends Base{

	
	ExtentReports extent;
    ExtentTest test;
    //ExtentReportListener EReport =new ExtentReportListener();
   
   
	Logger log;
	
	
	public AudienceWebApptest()
	{
		super();
	}

	@BeforeMethod
	public void setup()
	{
		initialization();
		
	}
	
	
	@BeforeTest
    public void config()
    {
		//EReport =new ExtentReportListener();
	//EReport.getClass();
    	
       extent = new ExtentReports(System.getProperty("user.dir") +"/test-output/Extent.html", true);
    }
	
	
	
	
	
	@Test(priority=1 ,enabled=false)
	public void Enter_Question() throws InterruptedException
	{
		
		 PigeonLabDashboard PLD=new PigeonLabDashboard();
		 AudienceWebApp AWA=new AudienceWebApp();
		// PigeonLabDashboard PLD=new PigeonLabDashboard();
			//log=Logger.getLogger("devpinoyLogger");
			
			//log.info("Brwoser opening ");
			
			//test.log(LogStatus.INFO, "broswe open");
			prop.getProperty("browser");
			//log.info("user clik on notification option");
			//test.log(LogStatus.INFO, "click on notification");
			PLD.click_Notification();
			PLD.click_Event();
			//log.info("user clik on event option");
			PLD.click_ConfirmationBox();
			System.out.print("user redirect to Evenet page ");
			Thread.sleep(3000);
			String ParanetWindow= driver.getWindowHandle();
			PLD.RunlinkBtn();
			PLD.Option_Audience_WebApp();
			Thread.sleep(5000);
			
			for(String childwindow:driver.getWindowHandles())
			{
				driver.switchTo().window(childwindow);
			}
			String ss=driver.getTitle();
	        System.out.println(ss);
	        driver.findElement(By.xpath("//a[@class='sessionlist-item-enter-button session-qna']")).click();
			Thread.sleep(4000);
	            
		Thread.sleep(4000);
		//AWA.ClickQA();
		AWA.enterquestion();
		AWA.AskButton();
		AWA.SubmitQuestion();
		
	}
	
	@AfterMethod 
	public void tearDown()
	{
		extent.flush();
		driver.quit();
	}
}
