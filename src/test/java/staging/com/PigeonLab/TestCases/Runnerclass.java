package staging.com.PigeonLab.TestCases;

import org.testng.TestNG;

import staging.com.ExtentReportListener.ExtentReportListener;

public class Runnerclass {

	
	static TestNG testNg;
	
	public static void main(String[] args) {
		
		ExtentReportListener ext = new ExtentReportListener();
		
		testNg =new TestNG();
		testNg.setTestClasses(new Class[]{PigeonLabDashboardTest.class});
		testNg.addListener(ext);
		testNg.run();
		
		
		
		
		
	}

}
